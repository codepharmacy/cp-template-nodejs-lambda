module.exports = {
	env: {
		browser: true,
		commonjs: true,
		es6: true,
		mocha: true
	},
	extends: ["airbnb-base", "prettier", "plugin:import/recommended"],
	globals: {
		Atomics: "readonly",
		SharedArrayBuffer: "readonly"
	},
	parserOptions: {
		ecmaVersion: 2018
	},
	plugins: ["import", "prettier", "chai-friendly"],
	rules: {
		indent: ["error", 2],
		"indent": [2, 2, { "SwitchCase": 1 }],
		"linebreak-style": ["error", "unix"],
		quotes: ["error", "double"],
		semi: ["error", "always"],
		"prettier/prettier": ["error"],
		curly: ["error", "all"],
		"no-confusing-arrow": "error",
		"no-console": "error",
		"no-unused-expressions": 0,
		"chai-friendly/no-unused-expressions": 2,
		"import/no-extraneous-dependencies": [
			"error",
			{ devDependencies: ["**/*.spec.js"] }
		]
	}
};
