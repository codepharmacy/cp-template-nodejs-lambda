const CORRELATION_ID = "CV-Correlation-Id";

const APPLICATION_VERSION = "CV-ApplicationVersion";

module.exports = {
  CORRELATION_ID,
  APPLICATION_VERSION
};
