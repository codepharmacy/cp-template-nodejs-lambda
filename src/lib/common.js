const { CORRELATION_ID, APPLICATION_VERSION } = require("./constants");

const { version: applicationVersion } = require("./../../package.json");

/**
 * Package the details into valid http response object
 * @param {Integer} statusCode the http status code for the response
 * @param {String} message a user friendly message to the caller understand the api response
 * @param {Object} payload upon success, contains any data that needs to be returned tp the caller
 * @param {Array<String>} errors upon failure, contains a list of errors responsible for the API failure
 */
const formattedResponse = ({
  statusCode,
  message,
  payload,
  errors,
  correlationId
}) => {
  const headers = {};

  headers[CORRELATION_ID] = correlationId;
  headers[APPLICATION_VERSION] = applicationVersion;

  return {
    statusCode,
    headers,
    body: JSON.stringify({
      message,
      payload,
      errors
    })
  };
};

module.exports = {
  formattedResponse
};
