/**
 * HTTP Status Codes
 * For more please refer to: https://confluence.sxm.telematics.net/confluence/pages/viewpage.action?spaceKey=CPSSPACE&title=API+Design+Guidelines
 */
const RESPONSE_CODE = {
  OK: 200,
  CREATED: 201,
  ACCEPTED: 202,
  NOT_MODIFIED: 304,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  CONFLICT: 409,
  PRECONDITION_FAILED: 412,
  UNPROCESSABLE_ENTITY: 422,
  PRECONDITION_REQUIRED: 428,
  INTERNAL_SERVER_ERROR: 500
};

/**
 * HTTP Response Messages
 * For more please refer to: https://confluence.sxm.telematics.net/confluence/pages/viewpage.action?spaceKey=CPSSPACE&title=API+Design+Guidelines
 */
const RESPONSE_MESSAGE = {
  SUCCESS: "The system succeeds to perform desired operation",
  NOT_FOUND: "Requested resource not found",
  VALIDATION_ERROR: "Errors found in the request",
  OPERATION_NOT_SUPPORTED: "The operation is not supported",
  OPERATION_NOT_PERMITTED: "The operation is not permitted",
  INTERNAL_SERVER_ERROR: "Error occurred while performing the operation"
};

module.exports = {
  RESPONSE_CODE,
  RESPONSE_MESSAGE
};
