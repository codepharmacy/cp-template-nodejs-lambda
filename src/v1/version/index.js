const { version } = require("./../../../package.json");
const { RESPONSE_CODE } = require("./../../lib/httpConstants");

/**
 * @swagger
 * /<?= repoName ?>-api/v1/version:
 *   get:
 *     summary: Get Version
 *     description: This operation will return current version of the app.
 *     tags:
 *       - Version
 *     produces:
 *       - text/plain
 *     parameters:
 *       - name: CV-Correlation-Id
 *         in: header
 *         type: string
 *     responses:
 *       200:
 *         description: OK
 *         schema:
 *           $ref: '#/definitions/SuccessResponse-Version'
 *       400:
 *         description: Bad Request
 *       401:
 *         description: Unauthorized
 *       403:
 *         description: Not Allowed
 *       500:
 *         description: Internal Server Error
 */
module.exports.endpoint = async () => {
  return {
    statusCode: RESPONSE_CODE.OK,
    headers: {
      "Content-Type": "text/plain"
    },
    body: version
  };
};
