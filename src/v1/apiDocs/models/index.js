/**
 * @swagger
 *
 * definitions:
 *   ErrorResponse-OperationNotPermitted:
 *     type: object
 *     required:
 *       - message
 *     properties:
 *       message:
 *         type: string
 *         example: Operation not permitted
 *       errors:
 *         type: array
 *         items:
 *           type: string
 *           example: The operation is not permitted
 *
 *   ErrorResponse-NotFound:
 *     type: object
 *     required:
 *       - message
 *     properties:
 *       message:
 *         type: string
 *         example: Requested resource not found
 *       errors:
 *         type: array
 *         items:
 *           type: string
 *           example: The operation is not supported
 *
 *   ErrorResponse-UnprocessableEntity:
 *     type: object
 *     required:
 *       - message
 *     properties:
 *       message:
 *         type: string
 *         example: Errors found in the request
 *       errors:
 *         type: array
 *         items:
 *           type: string
 *           example: operation must be one of [addition, subtraction, multiplication, division]
 *
 *   ErrorResponse-InternalError:
 *     type: object
 *     required:
 *       - message
 *     properties:
 *       message:
 *         type: string
 *         example: Internal Server Error
 *       errors:
 *         type: array
 *         items:
 *           type: string
 *           example: Error occurred while performing the operation
 *
 *   SuccessResponse-Version:
 *     type: string
 *     example: 0.0.1
 *
 */
