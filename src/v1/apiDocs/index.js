const {
  RESPONSE_CODE,
  RESPONSE_MESSAGE
} = require("./../../lib/httpConstants");

let swaggerJSON;

try {
  const path = "./api-docs.json";
  // eslint-disable-next-line import/no-dynamic-require, global-require
  swaggerJSON = require(path);
} catch (error) {
  // `api-docs not available: ${error.message}`
}

module.exports.endpoint = async () => {
  let response;

  if (swaggerJSON) {
    response = {
      statusCode: RESPONSE_CODE.OK,
      body: JSON.stringify(swaggerJSON)
    };
  } else {
    response = {
      statusCode: RESPONSE_CODE.NOT_FOUND,
      body: JSON.stringify({
        message: RESPONSE_MESSAGE.NOT_FOUND
      })
    };
  }

  return response;
};
