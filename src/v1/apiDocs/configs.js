const path = require("path");
const { name, version, description } = require("./../../../package.json");

// Swagger definition
const swaggerDefinition = {
  info: {
    title: name,
    description,
    version,
    license: {
      name: "Apache 2.0",
      url: "http://www.apache.org/licenses/LICENSE-2.0"
    }
  }
};

// Options for the swagger docs
const definitions = {
  ...swaggerDefinition,
  // Path to the API docs
  apis: [
    path.resolve(__dirname, "./../../v1/**/*.js"),
    path.resolve(__dirname, "./models/index.js")
  ]
};

module.exports = definitions;
