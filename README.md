# <?= repoName ?>

<?= projectDescription ?>

---

- [<?= repoName ?>](#<?= repoName ?>)
  - [APIs List](#apis-list)
    - [System APIs](#system-apis)
  - [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
      - [Installing Node.js](#installing-nodejs)
      - [Installing Serverless Framework](#installing-serverless-framework)
      - [Configuring AWS Credentials](#configuring-aws-credentials)
  - [Installing](#installing)
  - [Running the tests](#running-the-tests)
  - [Other Scripts](#other-scripts)
    - [Swagger 2.0 (OpenAPI) Docs Generator](#swagger-20-openapi-docs-generator)
    - [Code Linter](#code-linter)
    - [Code Coverage](#code-coverage)
    - [Generate Boilerplate code](#generate-boilerplate-code)
      - [Lambda and corresponding test suite](#lambda-and-corresponding-test-suite)
  - [Deployment](#deployment)
  - [Built With](#built-with)
  - [Versioning](#versioning)
  - [Project Structure](#project-structure)
  - [App Dependency Packages](#app-dependency-packages)
  - [Development Dependency Packages](#development-dependency-packages)
  - [Recommended Plugins for Visual Studio Code](#recommended-plugins-for-visual-studio-code)

---

## APIs List

### System APIs

-   [x] Swagger API Docs
-   [x] Application Version


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

What things you need to run the app and how to install/configure them:

```
* Node.js v10.17
* Serverless Framework
* AWS Credentials
```

#### Installing Node.js

-   Installing nvm (node version manager)

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
```

-   Open new terminal or reload the bash or shell

-   Installing and using Node.js v10.17

```
nvm install 10.17
nvm alias default 10.17
```

#### Installing Serverless Framework

```
npm -g install serverless
```

#### Configuring AWS Credentials

Please generate the token using `aws-scripts` project and ensure it is present in `~/.aws/credentials` file

## Installing

A step by step series of examples that tell you how to get the development env running

-   clone the project

```
git clone https://a-w2a-bitbkt001lv.na.atxglobal.com:8443/scm/cps/<?= repoName ?>.git
```

-   Setting node version

```
nvm use
```

-   Installing npm packages

```
npm install
```

-   Running the app locally

```
npm start
```

## Running the tests

The testing suite includes two kinds of tests:

-   **Unit tests** can be run using the following command:

```
npm test
```

## Other Scripts

### Swagger 2.0 (OpenAPI) Docs Generator

To generate the documentation, first ensure that the source code has proper comments, then run the following script:

```
npm run docs
```

### Code Linter

To analyze the source code to flag programming errors, bugs, stylistic errors, and suspicious constructs

```
npm run lint
```

### Code Coverage

To analyze how much of the source code is covered by the test suits before committing the code

```
npm run test-cov
```

**Note:** Every time, you run the above command, it generates a browser-friendly report at the path `coverage/lcov-report/index.html`

### Generate Boilerplate code

#### Lambda and corresponding test suite

```
sls create function -f <lambda_name> --handler src/v1/<lambda_name>/index.endpoint --path test/v1/ --httpEvent "get <?= repoName ?>-api/v1/<lambda_name>"
```

**Note:** The above command creates the test file with `<lambda_name>.js`. Please rename it manually to `<lambda_name>.spec.js`

## Deployment

Assuming you have generated token as mentioned in previous steps, run the following command to deploy the app:

-   Create CloudFormation Stack

```
sls deploy -s <env>
```

-   Delete CloudFormation Stack

```
sls remove -s <env>
```

where `env` can be `cft`, `devint`, `sit`, `stg` etc.

**Note:** Default `env` is set to `devint`

## Built With

-   [Serverless Framework](https://serverless.com/) - The framework used to develop and deploy and test the app
-   [Node.js](https://nodejs.org/en/) - The JavaScript runtime environment

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Project Structure

| Name                                | Description                                                     |
| ----------------------------------- | --------------------------------------------------------------- |
| **src/v1/apiDocs**/index.js         | Lambda that serves Swagger 2.0 (OpenAPI) docs                   |
| **src/v1/apiDocs**/api-docs.json    | Swagger 2.0 (OpenAPI) docs JSON                                 |
| **src/v1/apiDocs**/configs.js       | Base Swagger Definitions                                        |
| **src/v1/apiDocs**/models           | Swagger Common Model Definitions                                |
| **src/v1**/version                  | Lambda that returns current application version                 |
| **src/lib**/common.js               | Generic and shared functions                                    |
| **src/lib**/constant.js             | Common Store for all app constants                              |
| **src/lib**/httpConstants.js        | Common Store for all http response codes and messages           |
| **env**                             | Collection of tags, configs and parameters for each environment |
| **generator**/function-template.ejs | Boilerplate code to generate lambda                             |
| **generator**/test-template.ejs     | Boilerplate code to generate tests                              |
| **postman**/                        | Collection and Environment files to run the project via Postman |
| **test/lib**/                       | Unit test suites to test lib functions                          |
| **test/v1**/                        | Unit test suites to test lambdas                                |
| .eslintignore                       | Folder and files ignored by eslint linter                       |
| .eslintrc.js                        | Rules for eslint linter                                         |
| .gitignore                          | Folder and files ignored by git                                 |
| .nvmrc                              | NVM configuration file                                          |
| Jenkinsfile_CI                      | Jenkins file for CI/CD Workflow                                 |
| package-lock.json                   | Contains exact versions of NPM dependencies in package.json     |
| package.json                        | NPM dependencies                                                |
| README.md                           | Project Read Me file                                            |
| serverless.yml                      | Serverless Framework Configuration File                         |

## App Dependency Packages

| Package | Description |
| ------- | ----------- |

## Development Dependency Packages

| Package                     | Description                                                              |
| --------------------------- | ------------------------------------------------------------------------ |
| chai                        | BDD/TDD assertion library                                                |
| eslint                      | Linter JavaScript                                                        |
| eslint-config-airbnb-base   | Configuration eslint by airbnb                                           |
| eslint-config-prettier      | Turns off all rules that are unnecessary or might conflict with Prettier |
| eslint-plugin-chai-friendly | Makes eslint friendly towards Chai.js 'expect' and 'should' statements   |
| eslint-plugin-import        | ESLint plugin with rules that help validate proper imports               |
| eslint-plugin-prettier      | Runs Prettier as an ESLint rule                                          |
| nyc                         | Coverage test                                                            |
| prettier                    | Prettier is an code formatter                                            |
| prettier-eslint             | Prettier Eslint is an code formatter                                     |
| proxyquire                  | Proxies nodejs require in order to make overriding dependencies          |
| serverless-mocha-plugin     | Add support for test driven development using mocha                      |
| serverless-offline          | Emulates AWS λ and API Gateway for local development                     |
| swagger-jsdoc               | Generate OpenAPI (Swagger) specification                                 |

## Recommended Plugins for Visual Studio Code

* [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
* [Guides](https://marketplace.visualstudio.com/items?itemName=spywhere.guides)
* [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
* [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
* [Testify](https://marketplace.visualstudio.com/items?itemName=felixjb.testify)
* [openapi-designer](https://marketplace.visualstudio.com/items?itemName=philosowaffle.openapi-designer)